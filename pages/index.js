import React from "react";

import Navbar from "../components/homepage/Navbar";
import Header from "../components/homepage/Header";
import Main from "../components/homepage/Main";
import Featured from "../components/homepage/Featured";
import Footer from "../components/homepage/Footer";

export default function Home() {
  return (
    <div>
      <Navbar/>
      <Header/>
      <Main/>
      <Featured/>
      <Footer/>
    </div>
  )
}
